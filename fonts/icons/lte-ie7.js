/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icons\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-arrow-combo' : '&#x61;',
			'icon-basket' : '&#x62;',
			'icon-stackoverflow' : '&#x63;',
			'icon-menu' : '&#x64;',
			'icon-left-dir' : '&#x65;',
			'icon-down-dir' : '&#x66;',
			'icon-right-dir' : '&#x67;',
			'icon-up-dir' : '&#x68;',
			'icon-jquery' : '&#x31;',
			'icon-css3' : '&#x6a;',
			'icon-chevron-prev' : '&#x6b;',
			'icon-envelope' : '&#x6c;',
			'icon-cloud' : '&#x6d;',
			'icon-globe' : '&#x6e;',
			'icon-evernote' : '&#x30;',
			'icon-dropbox' : '&#x70;',
			'icon-wordpress' : '&#x71;',
			'icon-vimeo' : '&#x72;',
			'icon-cc' : '&#x73;',
			'icon-amazon' : '&#x74;',
			'icon-twitter' : '&#x75;',
			'icon-tumblr' : '&#x76;',
			'icon-behance' : '&#x77;',
			'icon-pinterest' : '&#x78;',
			'icon-linkedin' : '&#x79;',
			'icon-chat' : '&#x7a;',
			'icon-comment' : '&#x32;',
			'icon-like' : '&#x33;',
			'icon-instagram' : '&#x34;',
			'icon-map-marker' : '&#x35;',
			'icon-google-plus' : '&#x36;',
			'icon-github' : '&#x37;',
			'icon-chevron-next' : '&#x38;',
			'icon-folder' : '&#x39;',
			'icon-facebook' : '&#x6f;',
			'icon-skype' : '&#x21;',
			'icon-html5_logo' : '&#x40;',
			'icon-aim' : '&#x23;',
			'icon-smashing' : '&#x24;',
			'icon-image' : '&#x25;',
			'icon-clock' : '&#x5e;',
			'icon-tag' : '&#x26;',
			'icon-bookmark' : '&#x2a;',
			'icon-enter' : '&#x28;',
			'icon-exit' : '&#x29;',
			'icon-google-drive' : '&#x5f;',
			'icon-briefcase' : '&#x2d;',
			'icon-play' : '&#x2b;',
			'icon-eye' : '&#x3d;',
			'icon-search' : '&#x7b;',
			'icon-help' : '&#x7d;',
			'icon-info' : '&#x5b;',
			'icon-house' : '&#x5d;',
			'icon-phone' : '&#x5c;',
			'icon-pencil' : '&#x7c;',
			'icon-user' : '&#x3b;',
			'icon-code' : '&#x3a;',
			'icon-download' : '&#x27;',
			'icon-upload' : '&#x22;',
			'icon-disk' : '&#x2c;',
			'icon-book' : '&#x2e;',
			'icon-link' : '&#x2f;',
			'icon-calendar' : '&#x3c;',
			'icon-retweet' : '&#x3e;',
			'icon-reply' : '&#x3f;',
			'icon-lock' : '&#x60;',
			'icon-shuffle' : '&#x7e;',
			'icon-back' : '&#x5a;',
			'icon-warning' : '&#x42;',
			'icon-ccw' : '&#x4e;',
			'icon-cog' : '&#x4d;',
			'icon-rss' : '&#x58;',
			'icon-export' : '&#x46;',
			'icon-trash' : '&#x45;',
			'icon-file' : '&#x52;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};