boilerboot
==========

A bare-bones wordpress boilerplate to do themes with


## Here's what it's got:

- HTML5 Boilerplate
- Icons from Iconmoon
- Sass and Compass
- Bootstrap CDN
- Wholesome goodness

Some functions were added to functions.php

This thing should work from the get go, but it's missing the Wordpress stuff to make it an actual theme.
