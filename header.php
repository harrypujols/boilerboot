<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <!-- feed -->
	    <link href="<?php bloginfo('rss2_url'); ?>" title="RSS - <?php bloginfo('name'); ?>" type="application/rss+xml" rel="alternate">
	    <link href="<?php bloginfo('atom_url'); ?>" title="Atom - <?php bloginfo('name'); ?>" type="application/atom+xml" rel="alternate">
        <!-- favicon -->
	    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />

        <title><?php get_title(); ?></title>
        <meta name="description" content="<?php bloginfo('description'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>