<?php

//add scripts
function theme_scripts()
{
	//register scripts for our theme
	wp_register_script('mod', get_template_directory_uri() . '/js/vendor/modernizr.js', array( 'jquery' ), true );
	wp_register_script('bootstrap', '//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js', true );
	wp_register_script('main', get_template_directory_uri() . '/js/scripts.js', true );
	wp_enqueue_script( 'mod' );
	wp_enqueue_script( 'bootstrap');
	wp_enqueue_script( 'main' );
}

add_action( 'wp_enqueue_scripts', 'theme_scripts', 5 );

//add styles
function theme_styles()
{
	//register styles for our theme
	wp_register_style( 'main-style', get_template_directory_uri() . '/style.css', array(), 'all');
	wp_register_style( 'bootrap-style', '//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css', array(), 'all');
	wp_enqueue_style( 'main-style' );
	wp_enqueue_style( 'bootstrap-style' );
}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

// add thumbnail support
add_theme_support( 'post-thumbnails' ); 

//register sidebar widgets
  
if ( function_exists('register_sidebar') )
register_sidebar(array(
	'name'          => 'pages',
	'before_widget' => '<ul id="%1$s" class="side-nav widget %2$s">',
	'after_widget'  => '</ul>',
	'before_title'  => '<h4 class="widgettitle">',
	'after_title'   => '</h4>'
));

register_sidebar(array(
	'name'=>'posts',
	'before_widget' => '<ul id="%1$s" class="side-nav widget %2$s">',
	'after_widget'  => '</ul>',
	'before_title'  => '<h4 class="widgettitle">',
	'after_title'   => '</h4>'
));


//custom header support

$defaults = array(
	'default-image'          => '//placehold.it/960x315',
	'random-default'         => false,
	'width'                  => 960,
	'height'                 => 315,
	'flex-height'            => true,
	'flex-width'             => true,
	'default-text-color'     => '3fa8f2',
	'header-text'            => false,
	'uploads'                => true,
	// uncomment next line for callback for styling the header
	// 'wp-head-callback'       => 'fw_header_style',
);

add_theme_support( 'custom-header', $defaults );

// uncomment to load custom header options.
// require_once( get_template_directory() . '/custom-header.php' );

//custom background support
$defaults = array(
	'wp-head-callback'       => '_custom_background_cb'
);

add_theme_support( 'custom-background', $defaults );

//custom post formats support
$defaults = array(
	'image', 
	'aside',
	'gallery', 
	'status', 
	'quote', 
	'video',
	'link'
);

add_theme_support( 'post-formats', $defaults);
	
//register custom menus
function register_menus() {
	register_nav_menus( array(
		'header-menu' => __( 'Header Menu' ),
	));
}

add_action( 'init', 'register_menus' );

// uncomment bellow for custom search form
/*
function custom_search( $form ) {

    $form = '
    <form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search" />
    <button type="submit" id="searchsubmit"><i class="icon-search"></i></button>
    </form>
    ';

    return $form;
}

add_filter( 'get_search_form', 'custom_search' );
*/


// custom function for <title> tag

function get_title() {
	wp_title( '|', true, 'right' );
	// Add the site's name.
	bloginfo( 'name' );
	// Add the site's description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
}

// conditional browser class
function browser_detect(){
		$ipod = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iphone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$firefox = stripos($_SERVER['HTTP_USER_AGENT'],"Firefox");
		$safari = stripos($_SERVER['HTTP_USER_AGENT'],"Safari");
		$ie = stripos($_SERVER['HTTP_USER_AGENT'],"MSIE");
		$opera = stripos($_SERVER['HTTP_USER_AGENT'],"Opera");
		$chrome = stripos($_SERVER['HTTP_USER_AGENT'],"Chrome");
		//detecting device 
		if ($ipod == true || $iphone == true){
					echo "iphone";
		}
		elseif($firefox == true){
					echo "firefox";
		}
		elseif($ie == true){
					echo "explorer";
		}
		elseif($chrome == true){
					echo "chrome";
		}
		elseif($safari == true){
					echo "safari";
		}
		elseif($opera == true){
					echo "opera";
		}
		else{
			echo "unknown";
		}
	};

//end
?>
